defmodule AppWeb.Components.Layout.Navbar do
  use Surface.Component

  slot right

  slot default, required: true

  def render(assigns) do
    ~F"""
      <div class="flex flex-row inset-x-0 top-0 z-50 py-2 w-full transition duration-200 ease-in-out border-b border-base-200 bg-base-100 text-base-content sticky">
        <div class="basis-3/4">
          <#slot />
        </div>

        <div class="basis-1/4 text-right">
          <#slot name="right" />
        </div>
      </div>
    """
  end

end
