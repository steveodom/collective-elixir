defmodule AppWeb.Components.Form.InputGroup do
  use Surface.LiveComponent

  alias Surface.Components.Form.{ErrorTag, Label, TextInput}

  prop value, :string, required: true
  prop placeholder, :string
  prop focused, :string # left null instead of false.

  data touched_class, :string, default: "touched"

  def render(assigns) do
    ~F"""
      <div>
        <Label class="label text-slate-400 pb-0.5">{@placeholder}</Label>
        <div class="form-control" :hook="Autofocus" id={"input-container-#{@placeholder}-#{@focused}"}>
          <TextInput
            class="input input-bordered focus:outline-indigo-300"
            value={@value}
            opts={autofocus: @focused}
          />
          <ErrorTag class="text-red-500 mt-0.5 ml-2 text-sm"/>
        </div>
      </div>
    """
  end

end
