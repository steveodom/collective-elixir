defmodule App.Pages do
  @moduledoc """
  The Pages context.
  """

  import Ecto.Query, warn: false
  alias App.Repo
  alias App.Query

  alias App.Pages.Note
  alias App.Pages.Page

  def list(type, params) do
    Page
    |> where([page], page.type == ^type)
    |> filter_by_name(params["filter_text"])
    |> Query.sorted(params)
    |> Repo.all
  end

  def alphabetical(query) do
    from p in query,
    order_by: [asc: p.name]
  end

  def filter_by_name(model, q) do
    like_term = "%#{q}%"
    from p in model,
    where: like(p.name, ^like_term)
  end

  def starred do
    Page
    |> where([item], item.starred == true and item.active == true)
    |> Query.sorted(%{})
    |> Repo.all
  end

  def get!(id), do: Repo.get!(Page, id)

  def create(attrs \\ %{}) do
    %Page{}
    |> Page.changeset(attrs)
    |> Repo.insert()
  end

  def update(%Page{} = page, attrs) do
    IO.puts "updating the page"
    IO.inspect attrs
    page
    |> Page.changeset(attrs)
    |> Repo.update()
    # |> broadcast(:changed)
  end

  def delete(%Page{} = page) do
    Repo.delete(page)
  end

  def change(%Page{} = page, attrs \\ %{}) do
    Page.changeset(page, attrs)
  end

  def subscribe_starred do
    IO.puts "subscribed starred"
    Phoenix.PubSub.subscribe(App.PubSub, "starred")
  end

  # defp broadcast({:ok, page}, event) do
  #   IO.puts "broadcasting..."
  #   Phoenix.PubSub.broadcast(App.PubSub, "starred", {event})
  #   {:ok, page}
  # end


  @doc """
  Returns the list of notes.

  ## Examples

      iex> list_notes()
      [%Note{}, ...]

  """
  def list_notes do
    Repo.all(Note)
  end

  @doc """
  Gets a single note.

  Raises `Ecto.NoResultsError` if the Note does not exist.

  ## Examples

      iex> get_note!(123)
      %Note{}

      iex> get_note!(456)
      ** (Ecto.NoResultsError)

  """
  def get_note!(id), do: Repo.get!(Note, id)

  @doc """
  Creates a note.

  ## Examples

      iex> create_note(%{field: value})
      {:ok, %Note{}}

      iex> create_note(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_note(attrs \\ %{}) do
    %Note{}
    |> Note.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a note.

  ## Examples

      iex> update_note(note, %{field: new_value})
      {:ok, %Note{}}

      iex> update_note(note, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_note(%Note{} = note, attrs) do
    note
    |> Note.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a note.

  ## Examples

      iex> delete_note(note)
      {:ok, %Note{}}

      iex> delete_note(note)
      {:error, %Ecto.Changeset{}}

  """
  def delete_note(%Note{} = note) do
    Repo.delete(note)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking note changes.

  ## Examples

      iex> change_note(note)
      %Ecto.Changeset{data: %Note{}}

  """
  def change_note(%Note{} = note, attrs \\ %{}) do
    Note.changeset(note, attrs)
  end

end
