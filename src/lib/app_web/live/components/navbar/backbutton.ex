defmodule AppWeb.Components.Navbar.Backbutton do
  use Surface.Component

  alias Surface.Components.LiveRedirect
  alias Heroicons.Surface.Icon

  prop back_to, :string

  def render(assigns) do
    ~F"""
    <LiveRedirect to={@back_to}>
      <Icon name={"arrow-circle-left"} type="outline" class="h-8 w-8 inline align-top mr-2"/>
    </LiveRedirect>
    """
  end

end
