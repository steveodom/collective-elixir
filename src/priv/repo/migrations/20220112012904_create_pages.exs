defmodule App.Repo.Migrations.CreatePages do
  use Ecto.Migration

  def change do
    # citext is case-insenstive text
    # this enables the citext postgres extension
    execute "CREATE EXTENSION IF NOT EXISTS citext", "DROP EXTENSION citext"
    create table(:pages) do
      add :name, :citext
      add :type, :string
      add :attr1, :text
      add :attr2, :text
      add :attr3, :text
      add :html, :text
      add :last_edit, :timestamp
      add :active, :boolean, null: false, default: true
      add :deleted_at, :timestamp
      add :starred, :boolean, null: false, default: false
      timestamps()
    end
  end
end
