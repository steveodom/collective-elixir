defmodule AppWeb.Components.Navbar.Breadcrumb do
  use Surface.Component

  slot default, required: true
  # prop parent, :string, required: true

  def render(assigns) do
    ~F"""
      <div class="text-2xl breadcrumbs font-medium">
        <ul>
          <li>
            <#slot />
          </li>
        </ul>
      </div>
    """
  end

end
