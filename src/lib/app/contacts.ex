defmodule App.Contacts do
  # @moduledoc """
  # The Pages context.
  # """

  # import Ecto.Query, warn: false

  # alias Phoenix.PubSub
  # alias App.Repo
  # alias App.Query
  # alias App.Contacts.Contact

  # @topic inspect(__MODULE__)

  # # def subscribe do
  # #   PubSub.subscribe(App.PubSub, @topic)
  # # end

  # # def subscribe(user_id) do
  # #   Phoenix.PubSub.subscribe(App.PubSub, @topic <> "#{user_id}")
  # # end

  # def list(params) do
  #   Contact
  #   |> filter_by_name(params["filter_text"])
  #   |> Query.sorted(params)
  #   |> Repo.all
  # end

  # def starred do
  #   Contact
  #   |> where([item], item.starred == true and item.active == true)
  #   |> Query.sorted(%{})
  #   |> Repo.all
  # end

  # def filter_by_name(model, q) do
  #   like_term = "%#{q}%"
  #   from p in model,
  #   where: like(p.name, ^like_term)
  # end

  # # def by_email do
  # #   Contact
  # #   |> Query.alphabetical
  # #   |> Repo.all
  # # end

  # def get!(id), do: Repo.get!(Contact, id)

  # def create(attrs \\ %{}) do
  #   %Contact{}
  #   |> Contact.changeset(attrs)
  #   |> Repo.insert()
  # end

  # def update(%Contact{} = contact, attrs) do
  #   IO.puts "---------"
  #   IO.puts "contacts updating"
  #   IO.inspect contact
  #   IO.inspect attrs
  #   IO.puts "---------"
  #   contact
  #   |> Contact.changeset(attrs)
  #   |> Repo.update()
  # end

  # def delete(%Contact{} = contact) do
  #   Repo.delete(contact)
  # end

  # def toggle_starred(%Contact{id: id} = contact) do
  #   IO.puts "toggle star"
  #   IO.puts id
  #   broadcast_starred({:ok, contact}, :changed)
  # end

  # def subscribe_starred do
  #   IO.puts "subscribed starred"
  #   Phoenix.PubSub.subscribe(App.PubSub, "starred")
  # end

  # defp broadcast_starred({:ok, contact}, event) do
  #   IO.puts "broadcasting starred..."
  #   Phoenix.PubSub.broadcast(App.PubSub, "starred", {event, contact})
  #   {:ok, contact}
  # end

  # def change(%Contact{} = contact, attrs \\ %{}) do
  #   Contact.changeset(contact, attrs)
  # end

  # def notify_subscribers(result) do
  #   IO.puts "notifying"
  #   IO.inspect result
  #   # IO.inspect event
  #   PubSub.broadcast(App.PubSub, @topic, {:info, "Contacts updated"})
  #   # IO.inspect Process.info(self(), :messages)
  #   # Phoenix.PubSub.broadcast(App.PubSub, @topic <> "#{result.id}", {__MODULE__, event, result})
  #   {:noreply, result}
  # end

  # defp notify_subscribers({:error, reason}, _event), do: {:error, reason}

end
