# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     App.Repo.insert!(%App.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias App.Repo
alias App.Pages.Note
Repo.delete_all(Note)

notes = [
  %{
    title: "Note number 1",
    content: "<b>this is html</b>"
  },
  %{
    title: "Note number 2",
    content: "<b>this is more html</b>"
  }
]

Enum.each(notes, fn(data) ->
  %Note{}
  |> Note.changeset(data)
  |> Repo.insert!()
end)
# for note <- notes do
#   note
#   |> Note.changeset()
#   |> Repo.insert!()
# end
