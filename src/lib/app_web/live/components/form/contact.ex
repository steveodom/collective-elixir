defmodule AppWeb.Components.Form.Contact do
  use Surface.LiveComponent

  alias Surface.Components.Form
  alias Surface.Components.Form.{Field, TextInput}

  prop changeset, :changeset, required: true
  prop page, :any, required: true

  def render(assigns) do
    ~F"""
    <div>
      <Form for={@changeset} change={"save", target: :live_view} opts={autocomplete: "off"}>
        <Field name={:name}>
            <TextInput
            class="input w-full text-2xl p-0 focus:outline-0 focus:shadow-none"
            value={@page.name}
            opts={ autocomplete: "off" }
          />
        </Field>

        <Field name={:attr2}>
          <TextInput
            class="input w-full text-2xl p-0 focus:outline-0 focus:shadow-none"
            value={@page.attr2}
            opts={ autocomplete: "off", placeholder: "Phone" }
          />
        </Field>

        <Field name={:attr1}>
          <TextInput
            class="input w-full text-2xl p-0 focus:outline-0 focus:shadow-none"
            value={@page.attr1}
            opts={ autocomplete: "off", placeholder: "Email" }
          />
        </Field>
      </Form>
    </div>
    """
  end

end
