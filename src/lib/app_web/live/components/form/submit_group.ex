defmodule AppWeb.Components.Form.SubmitGroup do
  use Surface.Component

  alias Surface.Components.Form.{Submit}

  prop label, :string, default: "Save"
  prop loading_label, :string, default: "Saving"
  prop disabled, :boolean, default: false

  def render(assigns) do
    ~F"""
      <div class="my-6">
        <Submit
          label={@label}
          opts={
            phx_disable_with: @loading_label,
            disabled: @disabled
          }
          class="btn btn-primary"
        />
      </div>
    """
  end

end
