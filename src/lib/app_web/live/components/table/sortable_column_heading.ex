defmodule AppWeb.Components.Table.SortableColumnHeading do
  use Surface.LiveComponent
  alias Heroicons.Surface.Icon

  prop title, :string, required: true

  # the field name for this instance of the component
  prop field_name, :string, default: "name"

  # the current field that is being sorted on
  prop order_field, :string, default: "name"
  prop order_direction, :string, default: "down"
  prop sort_click, :event


  def render(assigns) do
    ~F"""
      <a class="hover:underline" :on-click={@sort_click} phx-value-order_field={@field_name} phx-value-order_direction={toggle_sort_direction(@order_direction)}>
        {@title} {#if @order_field == @field_name}<Icon name={"arrow-narrow-#{@order_direction}"} type="outline" class="h-4 w-4 inline align-top"/>{/if}
      </a>
    """
  end

  # Public API
  # def sort(dialog_id) do
  #   send_update(__MODULE__, id: dialog_id, show: true)
  # end

  defp toggle_sort_direction("up") do
    "down"
  end

  defp toggle_sort_direction("down") do
    "up"
  end
end
