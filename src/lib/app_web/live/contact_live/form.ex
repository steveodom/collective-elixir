defmodule AppWeb.ContactLive.Form do
  use Surface.LiveComponent

  alias Surface.Components.Form
  alias Surface.Components.Form.{Field, HiddenInput}

  alias AppWeb.Components.Form.{InputGroup, SubmitGroup}

  alias App.Pages

  prop action, :string, default: :new
  prop title, :string, default: "New Contact"
  prop contact, :any
  prop return_to, :string, default: "/contacts"

  data changeset, :any

  # def update(conn, params), do: throw params

  @impl true
  def update(%{contact: contact} = assigns, socket) do
    changeset = Pages.change(contact)
    IO.inspect changeset
    {:ok,
     socket
     |> assign(assigns)
     |> assign(:changeset, changeset)}
  end

  @impl true
  def handle_event("validate", %{"page" => page_params}, socket) do
    contact = socket.assigns.contact
    changeset =
      contact
      |> Pages.change(page_params)
      |> Map.put(:action, :validate)

      IO.inspect changeset
    {:noreply, assign(socket, :changeset, changeset)}
  end

  def handle_event("save", %{"page" => page_params}, socket) do
    save_contact(socket, socket.assigns.action, page_params)
  end

  def handle_event("save", %{"key" => "Enter"} = key, socket) do
    # this is needed to handle the Enter key. The save event gets triggered automatically
    {:noreply, socket}
  end

  defp save_contact(socket, :edit, page_params) do
    case Pages.update(socket.assigns.contact, page_params) do
      {:ok, _contact} ->
        {:noreply,
         socket
         |> put_flash(:info, "Contact updated successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}
    end
  end

  defp save_contact(socket, :new, page_params) do
    case Pages.create(page_params) do
      {:ok, _contact} ->
        {:noreply,
         socket
         |> put_flash(:info, "Contact created successfully")
         |> push_redirect(to: socket.assigns.return_to)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end

  @impl true
  def render(assigns) do
    ~F"""
      <Form for={@changeset} change="validate" submit="save" opts={
          autocomplete: "off",
          phx_key: "Enter",
          phx_window_keydown: "save"
        }
      >
        <HiddenInput field={:type} value="contact" />
        <Field name={:name}>
          <InputGroup value={@contact.name} focused="true" placeholder="Name" id="name-1"/>
        </Field>

        <Field name={:attr1}>
          <InputGroup value={@contact.attr1} id="email-1" placeholder="Email"/>
        </Field>

        <SubmitGroup disabled={!@changeset.valid?}/>
      </Form>
    """
  end
end
