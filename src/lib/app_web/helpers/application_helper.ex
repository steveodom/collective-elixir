defmodule AppWeb.ApplicationHelper do
  def empty?(nil), do: true
  def empty?(""), do: true
  def empty?(_), do: false

  def notEmpty?(nil), do: false
  def notEmpty?(""), do: false
  def notEmpty?(_), do: true
end
