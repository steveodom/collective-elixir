defmodule App.PagesFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `App.Pages` context.
  """

  @doc """
  Generate a note.
  """
  def note_fixture(attrs \\ %{}) do
    {:ok, note} =
      attrs
      |> Enum.into(%{
        content: "some content",
        title: "some title"
      })
      |> App.Pages.create_note()

    note
  end

  @doc """
  Generate a note.
  """
  def note_fixture(attrs \\ %{}) do
    {:ok, note} =
      attrs
      |> Enum.into(%{

      })
      |> App.Pages.create_note()

    note
  end

  @doc """
  Generate a contact.
  """
  def contact_fixture(attrs \\ %{}) do
    {:ok, contact} =
      attrs
      |> Enum.into(%{
        email: "some email",
        name: "some name"
      })
      |> App.Pages.create_contact()

    contact
  end
end
