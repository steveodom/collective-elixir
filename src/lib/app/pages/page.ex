defmodule App.Pages.Page do
  use Ecto.Schema
  import Ecto.Changeset
  @timestamps_opts [type: :utc_datetime]

  schema "pages" do
    field :active, :boolean, default: true
    field :attr1, :string
    field :attr2, :string
    field :attr3, :string
    field :deleted_at, :utc_datetime
    field :html, :string
    field :name, :string
    field :starred, :boolean
    field :type, :string
    timestamps()
  end

  @doc false
  def changeset(page, attrs) do
    page
    |> cast(attrs, [:type, :attr1, :attr2, :attr3, :name, :html, :starred])
    |> validate_required([:name])
  end

end
