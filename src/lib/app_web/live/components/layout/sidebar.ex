defmodule AppWeb.Components.Layout.Sidebar do
  @moduledoc """

  ## Examples
  """
  use AppWeb, :surface_live_component
  # use Surface.Component

  # alias App.Repo
  # alias App.Contacts.Contact

  alias AppWeb.Components.Layout.Sidebar.Starred

  # @impl true
  def render(assigns) do
    ~F"""
      <aside class="bg-slate-100 h-full flex flex-col justify-between border-r border-base-200 text-base-content w-80">
        <div class="sticky top-0 p-4 w-full ">
          <Starred id="starred-sidebar"/>
        </div>
      </aside>
    """
  end

  # @impl true
  def mount(socket) do
    {:ok, socket}
  end

  # # def update(assigns, socket) do
  # #   {:ok,
  # #     socket
  # #     |> assign(assigns)
  # #     |> assign(:count, count_contacts())
  # #   }
  # # end

  # defp count_contacts do
  #   Repo.aggregate(Contact, :count, :id)
  # end
end
