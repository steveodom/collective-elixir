defmodule AppWeb.ContactLive.Show do
  use AppWeb, :surface_live_view

  alias AppWeb.Components.Layout.Container
  alias AppWeb.Components.Layout.ContentWithRight
  alias AppWeb.Components.Layout.Sidebar.Starred
  alias AppWeb.Components.Navbar.{ Backbutton, Breadcrumb }
  alias AppWeb.Components.Actions.{ Star }
  alias AppWeb.Components.Editor

  alias AppWeb.Components.Tabs
  alias AppWeb.Components.Tabs.TabItem

  alias App.Pages


  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  # def update(assigns, socket) do
  #   IO.puts "updating show"
  #   IO.inspect socket
  #   # IO.inspect contact
  #   IO.puts "--d-d-d-d-d-d-d-d-"
  #   {:noreply, socket}
  #   # {:noreply, assign(socket, :contact, contact)}
  # end

  # def update(assigns, socket, contact) do
  #   IO.puts "updating contact"
  #   IO.inspect assigns
  #   IO.inspect contact
  #   {:noreply, socket}
  # end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    contact = Pages.get!(id)
    {:noreply,
     socket
     |> assign(:page_title, "Contact")
     |> assign(:contact, contact)
     |> assign(:starred, contact.starred)
     |> assign(:changeset, Pages.change(contact, %{}))
    }
  end

  @impl true
  def handle_event("save", %{"page" => contact_params}, socket) do
    contact = socket.assigns.contact
    changeset =
      contact
      |> Pages.change(contact_params)
      |> Map.put(:action, :save)
    {:noreply, assign(socket, :changeset, changeset)}

    # Normally the changeset is enough, but here we are saving it as well
    save_contact(socket, contact_params)
  end

  # @impl true
  # def handle_event("starToggle", _, socket) do
  #   # contact = socket.assigns.contact
  #   # toggled = !socket.assigns.starred
  #   # IO.inspect toggled
  #   IO.puts "what is toggled"
  #   IO.puts "----------------------------"

  #   # changeset =
  #   #   contact
  #   #   |> Pages.change(%{starred: toggled})
  #   #   |> Map.put(:action, :starToggle)
  #   send_update(Starred, id: "starred-sidebar", show: false)
  #   # save_contact(socket, %{starred: toggled})
  #   #
  #   # Contacts.toggle_starred(socket.assigns.contact)
  #   # Contacts.notify_subscribers(%{starred: toggled})
  #   # send_update(Starred, id: "starred-sidebar", show: false)
  #   # Phoenix.PubSub.broadcast(App.PubSub, "starred", {:change})
  #   # {:noreply, assign(socket, :starred, toggled)}
  #   {:noreply, socket}
  # end

  defp save_contact(socket, contact_params) do
    case Pages.update(socket.assigns.contact, contact_params) do
     {:ok, contact} ->

      if Map.has_key?(contact_params, "starred") do
        send_update(Starred, id: "starred-sidebar", show: false)
      end
      {:noreply, assign(socket, :contact, contact)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}
    end
  end

  @impl true
  def handle_info({:changed}, socket) do
    IO.puts "from sjpw"
    # IO.puts message
    # send_update(Starred, id: "starred-sidebar", show: false)
    {:noreply, socket}
  end
end
