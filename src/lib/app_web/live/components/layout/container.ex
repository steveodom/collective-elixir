defmodule AppWeb.Components.Layout.Container do
  @moduledoc """

  ## Examples
  """
  use Surface.Component

  slot default, required: true

  alias AppWeb.Components.Layout.Sidebar.Starred

  # @impl true
  def render(assigns) do
    ~F"""
      <div class="drawer drawer-mobile">
      <input id="main-menu" type="checkbox" class="drawer-toggle">
        <div class="drawer-side h-screen">
          <aside class="bg-slate-100 h-full flex flex-col justify-between border-r border-base-200 text-base-content w-80">
            <div class="sticky top-0 p-4 w-full ">
              <Starred id="starred-sidebar"/>
            </div>
          </aside>
        </div>
        <main class="flex-grow block overflow-x-hidden bg-base-100 text-base-content drawer-content">
          <div class="container px-4 py-0 h-full">
            <#slot/>
          </div>
        </main>
      </div>
    """
  end

  # @impl true
  def mount(socket) do
    {:ok, socket}
  end

end
