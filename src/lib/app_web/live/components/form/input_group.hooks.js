const Autofocus = {
  mounted(){
    const input = this.el.firstElementChild
    if (input.autofocus) {
      setTimeout(() =>{
        input.focus()
      }, 0)

    }
  }
};

export {Autofocus}