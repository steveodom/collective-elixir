defmodule AppWeb.Components.Layout.ContentWithRight do
  use Surface.Component

  slot right

  slot default, required: true

  def render(assigns) do
    ~F"""
      <div class="grid gap-1 grid-cols-12 h-full">
        <section class="col-span-10">
          <#slot />
        </section>

        <div class="bg-slate-100 col-span-2 -mr-4 p-4">
          <#slot name="right" />
        </div>
      </div>
    """
  end

end
