defmodule AppWeb.Components.Layout.Modal do
  use Surface.LiveComponent

  alias Heroicons.Surface.Icon

  prop title, :string, required: true
  prop ok_label, :string, default: "Ok"
  prop close_label, :string, default: "Close"
  prop ok_click, :event, default: "close"
  prop close_click, :event, default: "close"

  data show, :boolean, default: false

  slot default

  # Public API
  def open(dialog_id) do
    send_update(__MODULE__, id: dialog_id, show: true)
  end

  def close(dialog_id) do
    send_update(__MODULE__, id: dialog_id, show: false)
  end


  # Default event handlers
  @impl true
  def handle_event("close", _, socket) do
    {:noreply, assign(socket, show: false)}
  end

  @impl true
  def render(assigns) do
    ~F"""
      <div>
        <input type="checkbox" class="modal-toggle" checked={@show}>
        <div class="modal" :on-window-keydown={@close_click} phx-key="Escape">
          <div class="h-full w-full fixed bg-transparent" :on-click={@close_click}></div>
          <div class="modal-box z-1 max-h-full h-full rounded-none xl:max-w-screen-xl">
            <a :on-click={@close_click} class="link-neutral cursor-pointer">
              <Icon name="x" type="outline" class="h-6 w-6 inline align-top"/> <span class="text-slate-400">ESC</span>
            </a>
            <div class="max-w-screen-sm m-auto">
              <h3 class="text-lg leading-6 font-medium text-gray-900" id="modal-title">
                {@title}
              </h3>
              <div class="mt-2">
                <#slot />
              </div>
            </div>
          </div>
        </div>
      </div>
    """
  end

end
