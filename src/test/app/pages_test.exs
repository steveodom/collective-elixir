defmodule App.PagesTest do
  use App.DataCase

  alias App.Pages

  describe "notes" do
    alias App.Pages.Note

    import App.PagesFixtures

    @invalid_attrs %{content: nil, title: nil}

    test "list_notes/0 returns all notes" do
      note = note_fixture()
      assert Pages.list_notes() == [note]
    end

    test "get_note!/1 returns the note with given id" do
      note = note_fixture()
      assert Pages.get_note!(note.id) == note
    end

    test "create_note/1 with valid data creates a note" do
      valid_attrs = %{content: "some content", title: "some title"}

      assert {:ok, %Note{} = note} = Pages.create_note(valid_attrs)
      assert note.content == "some content"
      assert note.title == "some title"
    end

    test "create_note/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Pages.create_note(@invalid_attrs)
    end

    test "update_note/2 with valid data updates the note" do
      note = note_fixture()
      update_attrs = %{content: "some updated content", title: "some updated title"}

      assert {:ok, %Note{} = note} = Pages.update_note(note, update_attrs)
      assert note.content == "some updated content"
      assert note.title == "some updated title"
    end

    test "update_note/2 with invalid data returns error changeset" do
      note = note_fixture()
      assert {:error, %Ecto.Changeset{}} = Pages.update_note(note, @invalid_attrs)
      assert note == Pages.get_note!(note.id)
    end

    test "delete_note/1 deletes the note" do
      note = note_fixture()
      assert {:ok, %Note{}} = Pages.delete_note(note)
      assert_raise Ecto.NoResultsError, fn -> Pages.get_note!(note.id) end
    end

    test "change_note/1 returns a note changeset" do
      note = note_fixture()
      assert %Ecto.Changeset{} = Pages.change_note(note)
    end
  end

  describe "notes" do
    alias App.Pages.Note

    import App.PagesFixtures

    @invalid_attrs %{}

    test "list_notes/0 returns all notes" do
      note = note_fixture()
      assert Pages.list_notes() == [note]
    end

    test "get_note!/1 returns the note with given id" do
      note = note_fixture()
      assert Pages.get_note!(note.id) == note
    end

    test "create_note/1 with valid data creates a note" do
      valid_attrs = %{}

      assert {:ok, %Note{} = note} = Pages.create_note(valid_attrs)
    end

    test "create_note/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Pages.create_note(@invalid_attrs)
    end

    test "update_note/2 with valid data updates the note" do
      note = note_fixture()
      update_attrs = %{}

      assert {:ok, %Note{} = note} = Pages.update_note(note, update_attrs)
    end

    test "update_note/2 with invalid data returns error changeset" do
      note = note_fixture()
      assert {:error, %Ecto.Changeset{}} = Pages.update_note(note, @invalid_attrs)
      assert note == Pages.get_note!(note.id)
    end

    test "delete_note/1 deletes the note" do
      note = note_fixture()
      assert {:ok, %Note{}} = Pages.delete_note(note)
      assert_raise Ecto.NoResultsError, fn -> Pages.get_note!(note.id) end
    end

    test "change_note/1 returns a note changeset" do
      note = note_fixture()
      assert %Ecto.Changeset{} = Pages.change_note(note)
    end
  end

  describe "contacts" do
    alias App.Contact

    import App.PagesFixtures

    @invalid_attrs %{email: nil, name: nil}

    test "list_contacts/0 returns all contacts" do
      contact = contact_fixture()
      assert Pages.list_contacts() == [contact]
    end

    test "get_contact!/1 returns the contact with given id" do
      contact = contact_fixture()
      assert Pages.get_contact!(contact.id) == contact
    end

    test "create_contact/1 with valid data creates a contact" do
      valid_attrs = %{email: "some email", name: "some name"}

      assert {:ok, %Contact{} = contact} = Pages.create_contact(valid_attrs)
      assert contact.email == "some email"
      assert contact.name == "some name"
    end

    test "create_contact/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Pages.create_contact(@invalid_attrs)
    end

    test "update_contact/2 with valid data updates the contact" do
      contact = contact_fixture()
      update_attrs = %{email: "some updated email", name: "some updated name"}

      assert {:ok, %Contact{} = contact} = Pages.update_contact(contact, update_attrs)
      assert contact.email == "some updated email"
      assert contact.name == "some updated name"
    end

    test "update_contact/2 with invalid data returns error changeset" do
      contact = contact_fixture()
      assert {:error, %Ecto.Changeset{}} = Pages.update_contact(contact, @invalid_attrs)
      assert contact == Pages.get_contact!(contact.id)
    end

    test "delete_contact/1 deletes the contact" do
      contact = contact_fixture()
      assert {:ok, %Contact{}} = Pages.delete_contact(contact)
      assert_raise Ecto.NoResultsError, fn -> Pages.get_contact!(contact.id) end
    end

    test "change_contact/1 returns a contact changeset" do
      contact = contact_fixture()
      assert %Ecto.Changeset{} = Pages.change_contact(contact)
    end
  end
end
