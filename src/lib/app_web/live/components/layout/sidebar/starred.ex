defmodule AppWeb.Components.Layout.Sidebar.Starred do

  use Surface.LiveComponent

  alias App.Pages


  data pages, :list, default: []

  def render(assigns) do
    ~F"""
      <div>
        <h6>Starred Pages</h6>
        <span :for={item <- @pages} id={"page-#{item.id}"}>
          {item.name}
        </span>
      </div>
    """
  end

  def mount(socket) do
    # {:ok, socket}
    if connected?(socket), do: Pages.subscribe_starred()
    {:ok,
      socket
      |> assign(:pages, fetch_pages())
    }
  end

  def update(assigns, socket) do
    IO.puts "updating... starred are we?"
    IO.inspect assigns
    {:ok,
      socket
      |> assign(assigns)
      |> assign(:pages, fetch_pages())
    }
  end

  # def handle_info({App.Pages, {:info, message}}, socket) do
  #   IO.puts "from starred apppages"
  #   IO.puts message
  #   {:noreply, socket}
  #   # {:noreply, fetch(socket)}
  # end

  # @impl true
  # def handle_info({:changed}, socket) do
  #   IO.puts "from changed star"
  #   # IO.puts message
  #   # send_update(Starred, id: "starred-sidebar", show: false)
  #   {:noreply, socket}
  # end

  # # @impl true
  # def handle_info(_message, socket) do
  #   IO.puts "from starred"
  #   # IO.puts message
  #   {:noreply, socket}
  # end

  defp fetch_pages do
    starred = Pages.starred
    starred
  end

end
