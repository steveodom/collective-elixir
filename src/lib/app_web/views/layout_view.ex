defmodule AppWeb.LayoutView do
  use AppWeb, :view

  # Phoenix LiveDashboard is available only in development by default,
  # so we instruct Elixir to not warn if the dashboard route is missing.
  @compile {:no_warn_undefined, {Routes, :live_dashboard_path, 2}}

  def mount(socket) do
    socket = Surface.init(socket)
    {:ok, socket}
  end
end
