defmodule AppWeb.Components.Editor do
  use Surface.LiveComponent

  alias Surface.Components.Form
  alias Surface.Components.Form.{Field, HiddenInput}

  prop changeset, :any, required: true
  prop html, :string, required: true

  def render(assigns) do
    ~F"""
    <div :hook="Editor">
      <Form for={@changeset} change={"save", target: :live_view} opts={autocomplete: "off"}>
        <Field name={:html}>
          <HiddenInput id="x" value={@html} />
        </Field>
      </Form>
      <div id="source_product_description-editor" phx-update="ignore">
        <trix-editor class="border-0 text-lg trix-content prose lg:prose-xl" input="x" placeholder="click here to add notes and images."></trix-editor>
      </div>
    </div>
    """
  end

end
