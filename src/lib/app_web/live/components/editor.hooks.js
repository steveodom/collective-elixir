const bindTrix = function () {
  const trix = document.querySelector("trix-editor")

  if (trix != null) {
    trix.addEventListener("trix-change", function() {
      trix.inputElement.dispatchEvent(
        new Event("change", {bubbles: true})
      )
    })

    trix.addEventListener("trix-focus", function(event) {
      event.target.toolbarElement.style.display = "block"
    })

    trix.addEventListener("trix-blur", function(event) {
      event.target.toolbarElement.style.display = "none"
    })
  }
};

const Editor = {
  mounted(){
    bindTrix()
  },

  updated (){
		bindTrix()
  }
};

export {Editor}