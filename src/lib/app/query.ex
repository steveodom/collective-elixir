defmodule App.Query do
  @moduledoc """
  composable queries
  """

  import Ecto.Query, warn: false

  def sorted(model, params) do
    direction = params["order_direction"] || "down"
    field = params["order_field"] || "name"
    from p in model,
    order_by: ^filter_order_by(field, direction) # the pin operator ^ in ecto is used for query interpolation
  end

  defp filter_order_by("name", "up"), do: [desc: :name]
  defp filter_order_by("name", "down"), do: [asc: :name]
  defp filter_order_by("attr1", "down"), do: [asc: :attr1]
  defp filter_order_by("attr1", "up"), do: [desc: :attr1]
  defp filter_order_by(_field, _direction), do: []

end
