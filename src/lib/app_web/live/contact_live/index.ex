defmodule AppWeb.ContactLive.Index do
  use AppWeb, :surface_live_view
  import AppWeb.ApplicationHelper


  alias App.Pages
  alias App.Pages.Page

  alias AppWeb.Router.Helpers, as: Routes
  alias AppWeb.Components.Layout.Container

  alias Navbar
  alias AppWeb.Components.Navbar.Breadcrumb
  alias Modal

  alias AppWeb.Components.Table.SortableColumnHeading
  alias Heroicons.Surface.Icon

  alias AppWeb.ContactLive.Form, as: ContactForm


  @impl true
  def mount(_params, _session, socket) do
    {:ok,
      socket
      |> assign(:page, 1)
      |> assign(:order_direction, "down")
      |> assign(:order_field, "name")
      |> assign(:filter_text, "")
      |> assign(:contacts, list_contacts(%{}))}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply,
      socket
      |> assign(:page, params["page"] || socket.assigns.page)
      |> assign(:order_direction, params["order_direction"] || socket.assigns.order_direction)
      |> assign(:order_field, params["order_field"] || socket.assigns.order_field)
      |> apply_action(socket.assigns.live_action, params)}
  end

  defp apply_action(socket, :edit, %{"id" => id}) do
    Modal.open("edit_contact_dialog")
    socket
    |> assign(:page_title, "Edit Contact")
    |> assign(:contact, Pages.get!(id))
  end

  defp apply_action(socket, :new, _params) do
    Modal.open("new_contact_dialog")
    socket
    |> assign(:page_title, "New Contact")
    |> assign(:contact, %Page{})
  end

  defp apply_action(socket, :index, params) do
    socket
    |> assign(:page_title, "Contacts")
    |> assign(:contact, nil)
    |> assign(:contacts, list_contacts(params))
  end

  def handle_event("modal_closed", _, socket) do
    {:noreply,
      socket
        |> push_redirect(to: "/contacts")
    }
  end

  def handle_event("filter", params, socket) do
    {:noreply,
      socket
      |> assign(:filter_text, params["filter_text"] || "")
      |> apply_action(socket.assigns.live_action, params)
    }
  end

  def handle_event("open_form", _, socket) do
    Modal.open("form_dialog_1")
    {:noreply, socket}
  end

  @impl true
  def handle_event("delete", %{"id" => id}, socket) do
    contact = Pages.get!(id)
    {:ok, _} = Pages.delete(contact)
    {:noreply, assign(socket, :contacts, list_contacts(%{}))}
  end

  @impl true
  def handle_event("sort", params, socket) do
    {:noreply,
      socket
      |> assign(:order_direction, params["order_direction"] || socket.assigns.order_direction)
      |> assign(:order_field, params["order_field"] || socket.assigns.order_field)
      |> apply_action(socket.assigns.live_action, params)
    }
  end

  @impl true
  def handle_event("clearFilter", params, socket) do
    {:noreply,
      socket
      |> assign(filter_text: "")
      |> apply_action(socket.assigns.live_action, params)
    }
  end

  defp list_contacts(params) do
    Pages.list("contact", params)
  end

end
