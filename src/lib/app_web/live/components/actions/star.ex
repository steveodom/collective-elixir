defmodule AppWeb.Components.Actions.Star do
  use Surface.LiveComponent

  alias App.Pages

  alias Surface.Components.Form
  alias Surface.Components.Form.{Field, HiddenInput, Submit}

  prop changeset, :changeset, required: true
  prop page, :any, required: true

  def render(assigns) do
    ~F"""
      <Form for={@changeset} submit={"save", target: :live_view}>
        <Field name={:starred}>
          <HiddenInput id="x" value={"#{!@page.starred}"} />
        </Field>
        <Submit label={star_label(@page.starred)} class="btn btn-primary" />
      </Form>
    """
  end

  defp star_label(:true), do: "Unstar"
  defp star_label(:false), do: "Star"

end
