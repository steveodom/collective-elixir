module.exports = {
  mode: 'jit',
  content: [
    './js/**/*.js',
    './css/**/*.css',
    '../lib/*_web/**/*.*ex',
    '../lib/*_web/**/*.sface'
  ],
  theme: {
    extend: {
      colors: {
        'gray-light': '#EDF2F7'
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('daisyui'),
    require('@tailwindcss/typography')
  ],
  daisyui: {
    styled: true,
    themes: false,
    base: true,
    utils: true,
    logs: true,
    rtl: false,
  },
};
