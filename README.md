## Introduction

App I'm building to help me manage the build of my house. Learning Elixir/Phoenix in the process.

## Getting Started

It uses a [docker development enviroment](https://github.com/nicbet/docker-phoenix) for Phoenix projects.

I use an alias to start it;
`alias dmix.start="docker-compose up"`

Run mix with `./mix`. For example:
`./mix ecto.migrate`

```
### Database

To connect locally using Postico, I had to:
 - I had to include the ports: - "5432:5432" in docker
 - stop Postgres from running locally by stopping the app in the top bar.
 - I was able to specify localhost port 5432 and the db credentials in the Postico favorites.


#### Initialize the Database with Ecto

When you first start out, the `db` container will have no databases. Let's initialize a development DB using Ecto:

```
./mix ecto.create
```


## Notes

### Executing custom commands

To run commands other than `mix` tasks, you can use the `./run` script.

```
./run iex -S mix
```

## Building the image for your platform

You can locally build the container image with the included Makefile:

```sh
make docker-image
```
